// we created this @#^@#$^ file because the order of includes has become very strict. #$%^#$%^ !!!!

#include <Arduino.h>
#include <functional>

#include <FS.h>
#include <SPIFFS.h>

// ----------------------------------------------------------------------------
// -- useful macros for message display at various levels of importance -------
// ----------------------------------------------------------------------------

#define USBSERIAL	Serial	// used for messages while debugging the PortaProg code
#define UARTRPI	Serial1	// used to communicate with an attached device


//#define DEBUG(fmt, ...)		USBSERIAL.printf(fmt, ##__VA_ARGS__)
//#define VERBOSE(fmt, ...)		USBSERIAL.printf(fmt, ##__VA_ARGS__)
#define VERBOSEON(fmt, ...)		USBSERIAL.printf(fmt, ##__VA_ARGS__)
#define MESSAGE(fmt, ...)		screenPrintf(fmt, ##__VA_ARGS__)


#ifndef SCREENDEBUG
#define SCREENDEBUG(fmt, ...) ((void)0)
#endif
#ifndef AVRDEBUG
#define AVRDEBUG(fmt, ...) ((void)0)
#endif
#ifndef DEBUG
#define DEBUG(fmt, ...) ((void)0)
#endif
#ifndef VERBOSE
#define VERBOSE(fmt, ...) ((void)0)
#endif
#ifndef MESSAGE
#define MESSAGE(fmt, ...) ((void)0)
#endif


#include "pins.h"
#include "screen.h"
#include "buttons.h"
#include "uart.h"
