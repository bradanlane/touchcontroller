/* ***************************************************************************
* File:    main.cpp
* Date:    2019.09.09
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/


/* ---
# PortaProg

A portable programmer for both SPI and UPDI programmed microcontrollers.

This project has been developed for the TTGO T-Display board with a ST7789 display.
A future version may be created for the Adafruit ESP32 Feather.

![Portable Programmer](https://gitlab.com/bradanlane/portaprog/-/raw/master/files/portaprog.jpg)

These affordable boards are then augmented with a PortaProg receiver PCB which adds button, a power switch, necessary passives, and a breakout header.
 *(see `pins.h` documentation for details.)*

Supports:

- a common command set for ISP and UPDI programming
- direct firmware flash, dump, and read/write fuses over TCP
- flash using firmware stored on the SPIFFS
- TCP messages to the device UART (may be stored as command scripts)
- command scripts from SPIFFS (useful for smoketests)
- display of device UART data
- config file support
- startup script option
- file system and menu navigation using a 3-way toggle button
- actions buttons with script assignments

The combination of buttons to run command scripts and the send/receive of strings to a device attached to the UART allows for lots of options including running smoke tests and demos for attached devices.

The PortaProg will look for the `.config` file on the SPIFFS to perform initialization of WiFi, UART, and assignable CMD buttons.
If no `.config` file is found or if the specified WiFi is not available, the PortaProg will establish itself as an AP hotspot.

*NOTE: The PortaProg hardware is also suitable for building IoT devices or small portable games. The 2x6 pin header may be removed if not needed.*

**DISCLAIMER**: The PortaProg code evolved considerably as it was put into production for programming and testing the initial target projects.
During this phase, the priority was to keep those projects on schedule and this *may* have lead to some kludges and butchery of the PortaProg source code.
If you have a PortaProg, and wish to extend this code, you may discover it has some warts. You are free to fork the code. You are also encouraged to submit pull requests.

--------------------------------------------------------------------------
--- */



// local includes

#include "allincludes.h"
#include <driver/dac.h>


// -----------------------------------------------------------
// the standard Arduino entrypoints setup() and loop()
// -----------------------------------------------------------

void setup() {
	// Serial = default UART on ESP32
	USBSERIAL.begin(115200);
	while (!Serial)
		; // wait for serial attach

	delay(5000);	// KLUDGE need to wait for VSCode to swap to serial output

	USBSERIAL.println();
	USBSERIAL.println();
	USBSERIAL.println("USB Serial Initialized");


	// free up GPIO25 and GPIO26 pins
	dac_output_disable(DAC_CHANNEL_1);
	dac_output_disable(DAC_CHANNEL_2);


	screenInit();	// do this first so we have screen output capability
	buttonsInit();
	uartInit();

	USBSERIAL.println("---- System Initialized ----");

	//delay(1000);
	screenClear();
}

void loop() {
	uartLoop();
	buttonsLoop();
	screenLoop();

	delay(1);	// probably not needed
}

