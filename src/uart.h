/* ***************************************************************************
* File:    uart.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ***************************************************************************** */

/* ---
--------------------------------------------------------------------------

--- */



#ifndef __CMDS_H__
#define __CMDS_H__

// -------- CMDS dispatch functions --------------------------------------------------------------------
bool uartInit();
void uartLoop();
void uartSendCommand(char *cmd);

#endif
