#ifndef __BUTTON_H
#define __BUTTON_H

#define BUTTON_IDLE			0
#define BUTTON_PRESS		1
#define BUTTON_RELEASE		2
#define BUTTON_UNKNOWN		99

#define LONG_PRESS_DURATION 750 // milliseconds

#define BUTTON_LABEL_MAXLEN 64
#define BUTTON_CMD_MAXLEN 64
#define BUTTON_MAX 16


typedef struct {
	bool enabled;
	uint8_t state;
	bool dirty;
	uint16_t x, y, w, h;
	unsigned long debounce_timer;
	char label[BUTTON_LABEL_MAXLEN + 1];
	char cmd[BUTTON_CMD_MAXLEN + 1];
} Button;


bool	buttonsInit();
bool	buttonsLoop();

Button* buttonGet(uint8_t n);
bool 	buttonAdd(uint8_t n, uint16_t x, uint16_t y, uint16_t w, uint16_t h, char *label, char *cmd);
bool 	buttonRemove(uint8_t n);
bool 	buttonRemoveAll();

#endif

