#ifndef __PINS_H
#define __PINS_H

/* ***************************************************************************
* File:    pins.h
* Date:    2019.09.09
* Author:  Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

## HARDWARE

--- */


// exposed GPIO pins (X prefix means cant be shared for our purposes):
// 12, 13, 14, 16, 17, 33, 34(IN)
// X18(CLK), X19(MOSI), 23(MISO), 21(SDA), 22(SCL)
// 3.3V, GND

#define PIN_UART2_TX		14
#define PIN_UART2_RX		16

#endif	// end of include file
