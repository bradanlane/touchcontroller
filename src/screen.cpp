/* ***************************************************************************
 * File:    screen.cpp
 * Date:    2020.08.16
 * Author:  Bradan Lane Studio
 *
 * This content may be redistributed and/or modified as outlined
 * under the MIT License
 *
 * ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### SCREEN API

Provides a class to the device

The basic navigation uses 3 buttons which provide up, down, and select.
The file list and menu are navigated with up/down and th center button performs selection of the current file or menu action.
The NAV also performs forward/back scrolling of the virtual screen buffer with the physical display acting as a viewport.
The center button returns the screen to the previous mode.

The display and UI interface supports text-line based display functions.

The use of various graphics - such as a progress bar - could be future enhancements.


**DEVELOPER NOTE:** the coordinate system has 0,0 in the upper left.

--- */

//#define SCREENDEBUG(fmt, ...)	USBSERIAL.printf(fmt, ##__VA_ARGS__)

// now the local includes
#include "allincludes.h"
#include "screen.h"

#define HEX24_COLOR(r, g, b)	  (((((r) >> 3) & 0x1F) << 11) | ((((g) >> 2) & 0x3F) << 5) | ((((b) >> 3) & 0x1F))) // color is 5bits of red, 6 bits of green, and 5 bits of blue
#define COLOR_SCREEN_BACKGROUND	  TFT_BLACK

#define COLOR_HEADER_FOREGROUND	  TFT_WHITE
#define COLOR_HEADER_BACKGROUND	  COLOR_SCREEN_BACKGROUND

#define COLOR_MESSAGES_FOREGROUND TFT_ORANGE
#define COLOR_MESSAGES_BACKGROUND COLOR_SCREEN_BACKGROUND

#define COLOR_BUTTON_BACKGROUND HEX24_COLOR(0x00, 0x00, 0x3F)
#define COLOR_BUTTON_FOREGROUND HEX24_COLOR(0x7F, 0x7F, 0xFF)



// by tracking 'stored_size' and 'unread_size' we are able to rewind the buffer repeat using it
class Screen {
  private:
	// the output (screen) properties
	TFT_eSPI *display;
	int8_t active_font;

	// Create the touch object
	char banner[SCREEN_LINE_CHARS + 1];
	char lines_buffer[SCREEN_LINES][SCREEN_LINE_CHARS + 1];

	uint16_t current_display_line;				// number of lines in the buffer currently being used
	uint16_t current_display_line_char;		// current character position on the line
	uint16_t banner_height;
	uint16_t viewport_bottom;
	uint16_t viewport_lines;

	bool banner_dirty;
	bool buttons_dirty;
	bool display_dirty;
	bool display_wrap;

	uint16_t refresh_header();
	void refresh_soft_buttons();
	void refresh_current();
	void refresh_text();

  public:

	bool refresh_enabled;

	static const uint32_t DEFAULT_SIZE = SCREEN_LINE_CHARS;

	Screen(uint32_t buffer_size = Screen::DEFAULT_SIZE);
	~Screen();

	// write to display
	int print(const char*);

	// screen specific methods
	void refresh();
	void clear();
	void clearText();

	void buttonsDirty(bool rescan);
	void bannerSet(char *title);
};

// ---------------------------------------------------------------------------------------------------------------------------------------

uint16_t Screen::refresh_header() {
	uint16_t y_pos, x_pos;

	if (!(this->banner_dirty) && (this->banner_height > 0)) {
		return (this->banner_height);
	}

	//this->display->fillScreen(COLOR_SCREEN_BACKGROUND);

	// text positioning uses the top left of the font box

	// render header title area
	// menu title centered on top line
	if (this->active_font != HEADER_FONT) {
		this->display->setTextFont(HEADER_FONT);
		this->display->setTextSize(1);
		this->active_font = HEADER_FONT;
	}
	y_pos = 2;

	x_pos = (SCREEN_WIDTH - this->display->textWidth(this->banner)) / 2;
	this->display->fillRect(0, 0, SCREEN_WIDTH, this->display->fontHeight() + 4, COLOR_HEADER_BACKGROUND);

	this->display->setTextColor(COLOR_HEADER_FOREGROUND, COLOR_HEADER_BACKGROUND, true);
	this->display->setCursor(x_pos, y_pos);
	this->display->print(this->banner);

	x_pos = 0;
	y_pos = this->display->fontHeight() - 2;

	this->display->drawFastHLine(0, y_pos, SCREEN_WIDTH, TFT_WHITE);
	y_pos += 6; // position of next line of text

	this->banner_dirty = false;
	this->banner_height = y_pos;

	return (y_pos);
}

void Screen::refresh_soft_buttons() {
	if (!(this->buttons_dirty)) {
		VERBOSE("refresh_soft_buttons() nothing to do\n");
		return;
	}

	if (this->active_font != HEADER_FONT) {
		this->display->setTextFont(HEADER_FONT);
		this->display->setTextSize(1);
		this->active_font = HEADER_FONT;
	}

	for (int i = 0; i < BUTTON_MAX; i++) {
		Button *bptr = buttonGet(i);
		VERBOSEON("BTN #%02d: %s %s (%3d, %3d, %3d, %3d) %12s %s\n", i, (bptr->enabled ? "enabled ":"disabled"), (bptr->dirty ? "dirty" : "clean"), bptr->x, bptr->y, bptr->w, bptr->h, bptr->label, bptr->cmd);
		if ((bptr == NULL) || !(bptr->dirty))
			continue;

 		if (!(bptr->enabled)) {
			// special case, the button has been disabled but marked dirty so we erase the button
			if ((bptr->w > 0) && (bptr->h > 0))
				this->display->fillRect(bptr->x, bptr->y, bptr->w, bptr->h, COLOR_SCREEN_BACKGROUND);
		} else {
			if (bptr->state == BUTTON_PRESS) {
				this->display->fillRoundRect(bptr->x, bptr->y, bptr->w, bptr->h, (bptr->h / 6), COLOR_BUTTON_FOREGROUND);
				this->display->drawRoundRect(bptr->x, bptr->y, bptr->w, bptr->h, (bptr->h / 6), COLOR_BUTTON_BACKGROUND);
				this->display->setTextColor(COLOR_BUTTON_BACKGROUND, COLOR_BUTTON_FOREGROUND, true);
			} else {
				this->display->fillRoundRect(bptr->x, bptr->y, bptr->w, bptr->h, (bptr->h / 6), COLOR_BUTTON_BACKGROUND);
				this->display->drawRoundRect(bptr->x, bptr->y, bptr->w, bptr->h, (bptr->h / 6), COLOR_BUTTON_FOREGROUND);
				this->display->setTextColor(COLOR_BUTTON_FOREGROUND, COLOR_BUTTON_BACKGROUND, true);
			}

			char *label = bptr->label;
			// find center of button and then offset by half width and height of the label
			this->display->setCursor(
				bptr->x + ((bptr->w - this->display->textWidth(label)) / 2),
				bptr->y + ((bptr->h - this->display->fontHeight()) / 2) + 1);
			this->display->print(label);
		}
		bptr->dirty = false;
	}
	this->buttons_dirty = false;
}

void Screen::refresh_text() {
	if (this->refresh_enabled == false)
		return;

	uint16_t x_pos, y_pos;

	y_pos = this->refresh_header();	// this will give us the first usable point below the header

	// TODO need to optimize for use case where text added to line but not wrapped

	if (this->active_font != WINDOW_FONT) {
		this->display->setTextFont(WINDOW_FONT);
		this->display->setTextSize(1);
		this->active_font = WINDOW_FONT;
	}
	if (this->viewport_bottom < this->banner_height) {
		MESSAGE("Viewport size error: %d\n", this->viewport_bottom < this->banner_height);
		return;
	}
	this->viewport_lines = ((this->viewport_bottom - this->banner_height)) / this->display->fontHeight();
	if (this->viewport_lines > SCREEN_LINES)
		this->viewport_lines = SCREEN_LINES;
	VERBOSE("text area is from %3d to %3d\n", this->banner_height, this->viewport_bottom);

	this->display->setTextColor(COLOR_MESSAGES_FOREGROUND, COLOR_MESSAGES_BACKGROUND, true);

	for (int i=0; i < this->viewport_lines; i++) {
		// check if the line has any content
		if (this->lines_buffer[i][0]) {
			x_pos = 2;
			char *segment = this->lines_buffer[i];

			// look for our secret 'center me' character
			if (*segment == '`') {
				segment++; // skip the character
				// adjust the starting point to center the text
				x_pos = (SCREEN_WIDTH - this->display->textWidth(segment)) / 2;
				SCREENDEBUG("center line(%s) starting at %d\n", segment, x_pos);
			}
			this->display->fillRect(0, y_pos, SCREEN_WIDTH, (this->display->fontHeight()), COLOR_MESSAGES_BACKGROUND);
			this->display->setCursor(x_pos, y_pos);
			this->display->print(segment);

			SCREENDEBUG("[%03d,%03d](%2d): [%s]\n", x_pos, y_pos, strlen(segment), segment);
		} else {
			// clear blank lines
			this->display->fillRect(0, y_pos, SCREEN_WIDTH, (this->display->fontHeight()), COLOR_MESSAGES_BACKGROUND);
		}

		y_pos += (this->display->fontHeight());
	}
	SCREENDEBUG("\n");
}


/* ---
#### Screen::clear()

delete all current content and mark display buffer as dirty so it is refreshed during the next cycle

input: **bool** reset to _HOME_ mode
--- */
void Screen::clearText() {
	VERBOSE("Screen::clearText()\n");
	// if we have done a FLASH operation, then don't reset the display; we revert to MENU mode
	for (int i = 0; i < SCREEN_LINES; i++)
		memset(this->lines_buffer[i], 0, SCREEN_LINE_CHARS + 1);
	this->current_display_line = 0;
	this->current_display_line_char = 0;
	this->display_dirty = true;
}

void Screen::clear() {
	// TODO 'home' is now obsolete
	VERBOSEON("Screen::clear()\n");
	//this->refresh_enabled = true;
	this->clearText();
	this->display->fillScreen(COLOR_SCREEN_BACKGROUND);
	this->display_wrap = false;
	this->banner_height = 0;
	this->viewport_bottom = SCREEN_HEIGHT;
	this->viewport_lines = SCREEN_LINES;
	this->display_dirty = true;
	this->banner_dirty = true;
	this->buttonsDirty(true);
}

void Screen::buttonsDirty(bool rescan){
	this->buttons_dirty = true;

	if (rescan) {
		this->viewport_bottom = SCREEN_HEIGHT;
		for (int i=0; i < BUTTON_MAX; i++) {
			// shorten available viewport to not erase button
			Button *bptr = buttonGet(i);
			if (bptr->enabled) {
				if (bptr->y < this->viewport_bottom)
					this->viewport_bottom = bptr->y;
				}
				if ((bptr->w > 0) && (bptr->h > 0))
					bptr->dirty = true;
		}
		VERBOSE("new viewport bottom is %3d\n", this->viewport_bottom);
	}
}

void Screen::bannerSet(char *title) {
	if ((title == NULL) || (title[0] == 0)) {
		this->banner[0] = 0;
		// strcpy(this->banner, "UNINITIALIZED");
	} else {
		strcpy(this->banner, title);
	}
	this->banner_dirty = true;
}

/* ---
#### Screen::refresh()

repaint the display if is has been marked _dirty_ explicitly or by other operations

The display will automatically revert to the file list after a 90 second timeout without
updates or user interaction.
--- */
void Screen::refresh() {
	if (this->refresh_enabled == false)
		return;

	if (this->banner_dirty) {
		this->refresh_header();
	}
	if (this->buttons_dirty) {
		this->refresh_soft_buttons();
	}

	// provide and idle timeout to go 'home'
	if (this->display_dirty) {
		VERBOSE("Screen::refresh() !!NOP!!\n");
		this->display_dirty = false;
	}

}




int Screen::print(const char *line) {
	// output the message to serial by default
	VERBOSE("Screen::print(%s)\n", line);

	char *p = (char *)line;
	int count = 0;
	int most_recent_line = this->current_display_line;

	// for the various 'print' methods, we wrap long lines, wrap at a newline, and trim internal whitespace
	int wrapping_mode = 0; // 0=not_wrapping, 1=newline_wrapping, 2=end_of_line_buffer_wrapping
	bool padded = false;

	while (*p) {
		SCREENDEBUG("loop: %02X [%s]\n", *p, p);
		// if we hit a linefeed then ignore it
		if (*p == '\r') {
			p++;
			VERBOSE("skipping linefeed\n");
			continue;
		}
		if (*p == '`') {
			VERBOSE("FYI: we will center [%s]\n", p);
		}
		if (*p == '\n') {
			this->lines_buffer[this->current_display_line][this->current_display_line_char] = 0;
			this->current_display_line++;
			this->current_display_line_char = 0;
			wrapping_mode = 1;
			SCREENDEBUG("newline line wrap\n");
		} else if ((this->current_display_line_char >= SCREEN_LINE_CHARS) || (*p == '`')) {	// force wrap or our super secret 'center on line' character
			if (this->display_wrap) {
				this->lines_buffer[this->current_display_line][SCREEN_LINE_CHARS] = 0;
				this->current_display_line++;
				this->current_display_line_char = 0;
				wrapping_mode = 2;
				SCREENDEBUG("computed line wrap\n");
			}
			else {
				wrapping_mode = 0;
				SCREENDEBUG("preventing line wrap with [%02X]\n", *p);
				if (*p) {
					p++; // throw the character away
				}
				continue;
			}
		}

		if (*p == '\t')
			*p = ' ';

		// if we wrapped and wrapping lines is allowed, then store the character and we keep going
		if (wrapping_mode) {
			SCREENDEBUG("wrapping mode = %d\n", wrapping_mode);
			// if there are no more lines in our screen buffer, we shift it by 1 line to make room
			if (this->current_display_line >= this->viewport_lines) {
				VERBOSE("need to scroll the line buffer - line %d\n", this->current_display_line);
				// scroll to make room
				for (int i = 0; i < (SCREEN_LINES - 1); i++) {
					memcpy(this->lines_buffer[i], this->lines_buffer[i + 1], SCREEN_LINE_CHARS);
					SCREENDEBUG("[%02d] %s\n", i, this->lines_buffer[i]);
				}

				// set current line to the last line in the buffer
				this->current_display_line = (this->viewport_lines - 1);
				most_recent_line = 0; // need to invalidate optimization since we scrolled to reuse the last line

				// clear the last line buffer
				memset(this->lines_buffer[this->current_display_line], 0, SCREEN_LINE_CHARS);
				// this->lines_buffer[this->current_display_line][SCREEN_LINE_CHARS] = 0;
				SCREENDEBUG("[%02d] %s\n", this->current_display_line, this->lines_buffer[this->current_display_line]);
			}
			count++; // we consider wrapping as a byte to be counted
		}

		if ((*p) && (wrapping_mode != 1)) {
			this->lines_buffer[this->current_display_line][this->current_display_line_char] = *p;
			this->current_display_line_char++;
			count++;
		} else {
			SCREENDEBUG("skipping newline\n");
		}
		wrapping_mode = 0;

		if (*p) {
			p++; // advance to next character
		}
	} // end while

	SCREENDEBUG("\n");
	SCREENDEBUG("processed %d characters [%s]\n", count, p);
	this->display_dirty = true;
	this->refresh_text();

	return count;
}


// -------------------------------------------------------------------------
// ------------------------ Public Methods ---------------------------------
// -------------------------------------------------------------------------

/* ---
#### Screen::Screen()

Implements a compatible stream class where print() and write() operates on the device display.

--- */

Screen::Screen(uint32_t buffer_size) {
	VERBOSE("Screen::Screen\n");

	this->display = new TFT_eSPI();

	if (this->display != NULL) {
		this->display->init();
		// this->display->setRotation(1); // use the display wider than tall

		this->active_font = -1;
	} else {
		VERBOSE("Screen no display driver\n");
	}

	this->bannerSet(NULL);
	this->clear();
}

Screen::~Screen() {
}



// ---------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------

static Screen *g_screen;

bool screenInit(void) {
	 g_screen = new Screen(SCREEN_LINE_CHARS);
	if (!g_screen) {
		VERBOSEON("ERROR: failed to create Screen Stream ... so we are pretty much SOL\n");
	}
	return true;
}

void screenLoop(void) {
	g_screen->refresh();
	// delay(1);
}


// TODO markdown doc for the rest of the functions

void screenClear() {
	g_screen->clear();
}

void screenEnable() {
	g_screen->refresh_enabled = true;
}
void screenDisable() {
	g_screen->refresh_enabled = false;
}

void screenButtonsDirty(bool rescan) {
	g_screen->buttonsDirty(rescan);
}

void screenTitleSet(char *title) {
	g_screen->bannerSet(title);
}

int screenPrint(const char *text) {
	USBSERIAL.print(text); // most likely any formal output should also go to the debug screen.
	return g_screen->print(text);
}

int screenPrintf(const char *fmt...) {
	static char line[256 + 1];
	va_list args;
	va_start(args, fmt);

	line[0] = 0;
	vsnprintf(line, 256, fmt, args);
	line[256 - 1] = 0;
	va_end(args);

	return screenPrint(line);
}

