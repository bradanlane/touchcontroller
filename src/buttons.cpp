/* ***************************************************************************
* File:    buttons.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### BUTTONS API

Provides the button processing for the device UI.

--- */

#include "allincludes.h"
#include "ESP32Tone.h"

#define BUZZER_PIN 26

static Button g_buttons[BUTTON_MAX];

#include <FT6236.h> // FT6236 Touch Controller library
#define TOUCH_SCREEN_THRESHOLD 36
#define TOUCH_DEBOUNCE 250	// milliseconds
FT6236 touch_screen;

bool buttonsInit() {

	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		g_buttons[i].enabled = false;
		g_buttons[i].state = 0;
		g_buttons[i].dirty = false;
		g_buttons[i].label[0] = 0;
		g_buttons[i].cmd[0] = 0;
		g_buttons[i].x = 0;
		g_buttons[i].y = 0;
		g_buttons[i].w = 0;
		g_buttons[i].h = 0;
	}

	if (!touch_screen.begin(TOUCH_SCREEN_THRESHOLD)) {
		MESSAGE("Unable to start the capacitive touchscreen.\n");
	} else
		MESSAGE("capacitive touchscreen ready.\n");
	return true;
}

Button* buttonGet(uint8_t n) {
	if (n >= 0 && n < BUTTON_MAX)
		return &(g_buttons[n]);
	return NULL;
}

Button* buttonGetFree() {
	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		if (!(g_buttons[i].enabled))
			return &(g_buttons[i]);
	}
	return NULL;
}

bool buttonAdd(uint8_t n, uint16_t x, uint16_t y, uint16_t w, uint16_t h, char* label, char* cmd) {
	if ((n >= 0) && (n < BUTTON_MAX)) {
		g_buttons[n].x = x;
		g_buttons[n].y = y;
		g_buttons[n].w = w;
		g_buttons[n].h = h;
		strcpy(g_buttons[n].label, label);
		strcpy(g_buttons[n].cmd, cmd);
		g_buttons[n].state = 0;
		g_buttons[n].enabled = true;
		g_buttons[n].dirty = true;
		g_buttons[n].debounce_timer = 0;
		screenButtonsDirty(true);
		VERBOSEON("ADD%d: %s (%3d, %3d, %3d, %3d)\n", n, g_buttons[n].label, g_buttons[n].x, g_buttons[n].y, g_buttons[n].w, g_buttons[n].h);
	}
	return true;
}

bool buttonRemove(uint8_t n) {
	if ((n >= 0) && (n < BUTTON_MAX)) {
		VERBOSE("DEL: %s (%3d, %3d, %3d, %3d)\n", g_buttons[n].label, g_buttons[n].x, g_buttons[n].y, g_buttons[n].w, g_buttons[n].h);
		g_buttons[n].x = 0;
		g_buttons[n].enabled = false;
		g_buttons[n].state = 0;
		g_buttons[n].dirty = true;
		screenButtonsDirty(true);
	}
	return true;
}

bool buttonRemoveAll() {
	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		g_buttons[i].enabled = false;
		g_buttons[i].state = 0;
		g_buttons[i].dirty = false;
		g_buttons[i].label[0] = 0;
		g_buttons[i].cmd[0] = 0;
		g_buttons[i].x = 0;
		g_buttons[i].y = 0;
		g_buttons[i].w = 0;
		g_buttons[i].h = 0;
	}
	//screenButtonsDirty(true);
	screenClear();
	return true;
}

/* ---
#### buttonsUpdate()

*Internal function* Return the current button state

The range of button states allows the caller to track both the start of a press event or long press as well as the eventual release.

 The button states are:

- IDLE - the button has no interaction
- PRESS - the button has been pressed
- LONG PRESS - the button has been pressed and held down for more than 750 ms
- RELEASE - the button has been released after being pressed
- LONG RELEASE - the button has been released after being held for more than 750 ms
- UNKNOWN - something went wrong ... probably because the programmer of this code didn't have enough espresso and was asleep at the wheel

**Note**: When this function is called from multiple places, the status will reflect the change between calls, regardless of caller.  *(this is a polling function, not interrupt driven)*

 return: **bool** true if any buttons have changed state
--- */

bool buttonsLoop() {
	bool dirty = false;
	TS_Point p;
	int touches;

	// read the touch display
	if ((touches = touch_screen.touched())) {
		p = touch_screen.getPoint();
		// Retrieve a point
		// display is 320 wide x 480 tall
		// buttons along the bottom
		VERBOSE("Touch: %3d %3d\n", p.x, p.y);
	}

	// check if touch is within any of the active buttons
	int button_num = -1;
	if (touches) {
		for (uint8_t i = 0; i < BUTTON_MAX; i++) {
			if (g_buttons[i].enabled == false)
				continue;

			if (((p.x >= g_buttons[i].x) && (p.x < g_buttons[i].x + g_buttons[i].w)) &&
				((p.y >= g_buttons[i].y) && (p.y < g_buttons[i].y + g_buttons[i].h)))
			{
				button_num = i;
				break;
			}
		}
	}
	if (button_num >= 0) {
		VERBOSE("Button #%d %s activated by press (%3d, %3d)\n", button_num, g_buttons[button_num].label, p.x, p.y);
	} else if (touches) {
		VERBOSE("No button activated by press (%3d, %3d)\n", p.x, p.y);
	}

	// process all buttons
	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		if (!g_buttons[i].enabled) {
			continue;
		}

		bool active = false;
		if (i == button_num)
			active = true;

		if (active) {
			switch (g_buttons[i].state) {
				case BUTTON_IDLE: {
					if (!(g_buttons[i].debounce_timer) || (g_buttons[i].debounce_timer < millis())) {
						g_buttons[i].state = BUTTON_PRESS;
						VERBOSEON("PRESS [%s]\n", g_buttons[i].label);
						g_buttons[i].dirty = true;
						g_buttons[i].debounce_timer = 0;
						dirty = true;
						tone(BUZZER_PIN, 220);
					}
				} break;
				default: {
					//current_state = BUTTON_UNKNOWN;
				} break;
			}
		} else {
			switch (g_buttons[i].state) {
				case BUTTON_PRESS: {
					g_buttons[i].state = BUTTON_RELEASE;
					VERBOSEON("RELEASE [%s]\n", g_buttons[i].label);
					g_buttons[i].dirty = true;
					dirty = true;
					g_buttons[i].debounce_timer = millis() + TOUCH_DEBOUNCE;
					noTone(BUZZER_PIN);
				} break;
				case BUTTON_RELEASE: {
					g_buttons[i].state = BUTTON_IDLE;
					VERBOSEON("IDLE [%s]\n", g_buttons[i].label);
					//dirty = true;
				} break;
				default: {
					//current_state = BUTTON_UNKNOWN;
				} break;
			}
		}
	}

	if (dirty) {
		// in most cases we are watching for button releases
		for (int i = 0; i < BUTTON_MAX; i++) {
			if (g_buttons[i].enabled) {
				if (g_buttons[i].state == BUTTON_RELEASE) {
					VERBOSEON("Button [%s] Action [%s]", g_buttons[i].label, g_buttons[i].cmd);
					uartSendCommand(g_buttons[i].cmd);
				}
			}
		}
		screenButtonsDirty(false);
	}
	return dirty;
}
