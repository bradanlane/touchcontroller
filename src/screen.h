#ifndef __Screen_H
#define __Screen_H

#include <TFT_eSPI.h>
// This sketch uses font files created from the Noto family of fonts:
// https://www.google.com/get/noto/

#define HEADER_FONT 4
#define WINDOW_FONT 2

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 480

#define SCREEN_LINES	  	60
#define SCREEN_LINE_CHARS 	48 // actually its only an estimate since the font is proportional

bool screenInit(void);
void screenLoop(void);
void screenClear();

void screenDisable();
void screenEnable();

void screenButtonsDirty(bool rescan);
void screenTitleSet(char *title);
int screenPrint(const char *text);
int screenPrintf(const char *fmt...);

#endif
