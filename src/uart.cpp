/* ***************************************************************************
 * File:    uart.cpp
 * Date:    2020.08.16
 * Author:  Bradan Lane Studio
 *
 * This content may be redistributed and/or modified as outlined
 * under the MIT License
 *
 * ***************************************************************************** */

/* ---
--------------------------------------------------------------------------

## UART - control interface

--------------------------------------------------------------------------

--- */

//#define DEBUG(fmt, ...)		USBSERIAL.printf(fmt, ##__VA_ARGS__)

#include "allincludes.h"

/*
	commands may any of the following:
		discrete commands
		commands with one or more parameter values
		commands with stream content (eg receiving a file)

	a single line may have multiple commands
	there may be multiple lines
	a stream must be the last line as there is no way of knowing when the file ends
*/





// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

static bool __trim(char *line) {
	for (int i = strlen(line) - 1; i > 0; i--) {
		if ((line[i] == '\n') || (line[i] == '\r') || (line[i] == '\t') || (line[i] == ' '))
			continue;
		line[i + 1] = 0;
		return true;
	}
	return false;
}

static bool _is_printable(char *line) {
	// sanity check: make sure there are printable characters available
	if (!strlen(line))
		return true;
	bool printable = false;
	for (int i = 0; i < strlen(line); i++) {
		if (isprint(line[i])) {
			printable = true;
			break;
		}
	}
	return printable;
}

#define MAX_UART_BUF 256
static char g_uart_buffer[MAX_UART_BUF + 1];
uint16_t g_uart_data_len;
bool g_uart_eol;



uint16_t __strip_nl(char* b, uint16_t n) {
	bool more = true;
	do {
		if (n && ((b[n - 1] == '\n') || (b[n - 1] == '\r'))) {
			n--;
			b[n] = 0;
		} else {
			more = false;
		}
	} while (more);
	return n;
}

static bool __uart_read() {
	bool eol = false;

	while (UARTRPI.available() && !eol) {
		char data = UARTRPI.read();
		if (data) {
			g_uart_buffer[g_uart_data_len] = data;
			g_uart_data_len++;
			DEBUG("UART: [%02X]\n", data);
			if (g_uart_data_len >= MAX_UART_BUF) {
				g_uart_buffer[MAX_UART_BUF] = 0;
				eol = true;
			}
			if ((data == 0x0D) || (data == 0x0A)) { // new line
				g_uart_buffer[g_uart_data_len] = 0;
				eol = true;
			}
		}
	}
	if (eol) {
		__strip_nl(g_uart_buffer, g_uart_data_len);
		g_uart_data_len = 0; // reset where next byte of data will go
	}
	return eol;
}



void uartSendCommand(char* cmd) {
	UARTRPI.printf("[C]%s\n", cmd);
}

/* --
#### uartInit()

Performs all necessary initialization of programming systems.
Must be called once before using any of the other AVR operations.

- return: **bool** `true` on success and `false` on failure

-- */
bool uartInit() {

	g_uart_buffer[0] = 0;
	g_uart_data_len = 0;
	g_uart_eol = false;

	// first time
	delay(10);
	// UARTRPI is the second available (third overall) UART on the ESP32 - used for communicating to an ECC
	UARTRPI.begin(115200, SERIAL_8N1, PIN_UART2_RX, PIN_UART2_TX);
	while (!UARTRPI)
		;								 // wait for serial attach
	pinMode(PIN_UART2_RX, INPUT_PULLUP); // set this may solve noise on the RX when no external device is connected

	// we may get a bit of garbage from the first button push so let's clear it now
	delay(2);
	UARTRPI.flush();

	VERBOSE("uart_setup %s\n", (UARTRPI ? "success" : "failed"));
	return (UARTRPI ? true : false);
}


/* --
#### uartLoop()

Give the uart processing time

-- */

void uartLoop() {
	bool found;
	found = __uart_read();

	if (found) {
		VERBOSE("UART: [%s]\n", g_uart_buffer);
		bool processed = false;
		/*	The UART data may be one of two types
				- simple console output
				- a command
			console output is rendered on the screen
			command strings need to be processed
			commands include:
				create a button		"[A] n x y w h label command"
				delete a button		"[D] n"
				set the banner		"[T] label"
				reset everything	"[R]"
				display off			"[-]"
				display on			"[+]"
			Note: when buttons are added or deleted, the remaining space for console text must be recomputed
		*/

		int len = strlen(g_uart_buffer);
		// is this a command sequence ?
		if ((len > 2) && (g_uart_buffer[0] == '[') && (g_uart_buffer[2] == ']')) {
			switch (g_uart_buffer[1]) {
				case 'A': {
					int n, x, y, w, h, used;
					char label[BUTTON_LABEL_MAXLEN + 1], cmd[BUTTON_CMD_MAXLEN + 1];
					if (sscanf(&(g_uart_buffer[4]), "%d %d %d %d %d %s %n", &n, &x, &y, &w, &h, label, &used) != 6) {
						MESSAGE("Error Parsing Sequence: [%s]", g_uart_buffer);
						break;
					}
					strcpy(cmd, &(g_uart_buffer[4 + used]));
					VERBOSEON("used=%d  remainder is [%s]\n", used, cmd);
					// WARNING cmd can not have spaces ... should fix this but not for now
					// UARTRPI.printf("Add Button #%d: (%3d,%3d, %3d, %3d) [%s]=[%s]\n", n, x, y, x+w, y+h, label, cmd);
					buttonAdd(n, x, y, w, h, label, cmd);
					processed = true;
				} break;
				case 'D': {
					int n;
					if (sscanf(&(g_uart_buffer[4]), "%d", &n) != 1) {
						MESSAGE("Error Parsing Sequence: [%s]", g_uart_buffer);
						break;
					}
					//UARTRPI.printf("Remove Button #%d\n", n);
					buttonRemove(n);
					processed = true;
				} break;
				case 'R': {
					int n;
					buttonRemoveAll();
					screenTitleSet(NULL);
					processed = true;
				} break;
				case 'T': {
					//UARTRPI.printf("New Title: [%s]\n", &(g_uart_buffer[4]));
					screenTitleSet(&(g_uart_buffer[4]));
					processed = true;
				} break;
				case '-': {
					screenDisable();
					processed = true;
				} break;
				case '+': {
					screenEnable();
					processed = true;
				} break;
				default: {
					MESSAGE("Unrecognized sequence: [%s]", g_uart_buffer);
				} break;
			}
		}
		if (!processed) {
			MESSAGE("%s\n", g_uart_buffer);
			//UARTRPI.printf("ACK: [%s]\n", g_uart_buffer);
		}
	}
}
