'''
UART communication on Raspberry Pi using Pyhton
http://www.electronicwings.com
'''

import time
import subprocess
import serial
import io
import importlib
from touchcontroller import TouchController

# create our UI interface
# Title Length is about 14 characters

# WARNING: labels and commands currently do not support whitespace
labels = (
    'One', 'Two', 'Three',
    'Four', 'Five', 'Six',
    'Seven', None, 'EXIT'
)
commands = (
    '[I]_template', '[I]_template', '[I]_template',
    '[I]_template', '[I]_template', '[I]_template',
    '[I]_template', None, 'exit'
)

controller = TouchController('PCB Tester', 3, 3, labels, commands)

more = True
while more:
    num = controller.update()
    if num >= 0:
        # spawn command in subprocess and capture output
        if num < len(labels)-1:
            controller.run(num)
        elif num == len(labels)-1:
            # our exit command
            del controller
            more = False
        else:
            print('ERROR: unrecognized command %d' % (num))
            pass # FYI: this should not be possible and means a bug in the code
