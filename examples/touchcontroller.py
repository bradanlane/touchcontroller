import time
import subprocess
import importlib
import serial
import io

class TouchController:
    SCREEN_WIDTH = 320
    SCREEN_HEIGHT = 480
    BUTTON_GAP = 4
    BUTTON_HEIGHT = 80

    def __init__(self, title, rows, cols, labels, commands):
        self._serial_connection = serial.Serial(port='/dev/ttyAMA2', baudrate=115200, timeout=0.25)
        self._serial_io = io.TextIOWrapper(io.BufferedRWPair(self._serial_connection, self._serial_connection))

        self.configure(title, rows, cols, labels, commands)

    def __del__(self):
        # clear the existing UI
        self.writeline('[R]')
        time.sleep(0.5)
        del self._serial_io
        del self._serial_connection
        time.sleep(0.5)


    def configure(self, title, rows, cols, labels, commands):
        if (len(labels) != len(commands)) and (len(labels) != (rows * cols)):
            # there is a mispatch between teh number of buttons and the labels:commands to assign
            raise ValueError('number of labels or commands does not match number of buttons')

        self._rows = rows
        self._cols = cols
        self._bwidth = ((TouchController.SCREEN_WIDTH - (TouchController.BUTTON_GAP * (cols - 1))) // cols)
        self._bheight = TouchController.BUTTON_HEIGHT

        self._title = title
        self._labels = labels
        self._commands = commands
        self._imports = [None] * (rows*cols)

        print('%s: ' % (title), labels)

        # turn screen refresh off
        self.writeline('[-]')
        # clear the existing UI
        self.writeline('[R]')
        # set banner title
        self.writeline(('[T] %s' % (title)))
        # create buttons
        for r in range(rows):
            y = TouchController.SCREEN_HEIGHT - ((rows-r) * (self._bheight + TouchController.BUTTON_GAP))
            for c in range(cols):
                n = (r * cols) + c
                x = (c * (self._bwidth + TouchController.BUTTON_GAP))
                if self._labels[n] is not None:
                    self.writeline('[A] %d  %d %d %d %d  %s %s' % (n, x, y, self._bwidth-1, self._bheight, self._labels[n], self._commands[n]))
        # turn screen refresh back on
        self.writeline('[+]')

    def loop(self):
        # for simple use cases, we provide the entire loop proccessing
        more = True
        while more:
            num = self.update()
            if num >= 0:
                # spawn command in subprocess and capture output
                if num < len(self._labels)-1:
                    self.run(num)
                elif num == len(self._labels)-1:
                    more = False
                    pass
                else:
                    print ("unrecognized command %d" % (num))

    def update(self):
        line = self.readline()
        if line:
            if line.startswith('[C]'):
                line = line[3:]
                #print('received command: %s' % (line))
                try:
                    pos = self._commands.index(line)
                    print('received command #%d' % (pos))
                    return pos
                except ValueError as e:
                    print('Error: command [%s] not found (%s)' % (line, e))
                    print('       known commands are:', self._commands)
            else:
                print('unrecognized message: %s' % (line))
            pass
        return -1

    def readline(self):
        line = self._serial_io.readline()
        if line:
            line = line.strip()
        return line

    def writeline(self, line):
        self._serial_io.write(line)
        self._serial_io.write('\r')
        self._serial_io.flush()
        time.sleep(0.1)

    def run(self, num):
        if num < 0 or num >= len(self._labels):
            return

        command = self._commands[num]

        ## run it ##
        if command.startswith('[I]'):
            name = command[3:]
            #print('name = %s' % (name))
            if self._imports[num] is None:
                #print("import child-module '%s' ..." % (command))
                self._imports[num] = importlib.import_module(name)
            # save our UI
            saved_title = self._title
            saved_labels = self._labels
            saved_commands = self._commands
            saved_rows = self._rows
            saved_cols = self._cols
            saved_imports = self._imports
            #print("call child-module '%s' ..." % (command))
            self._imports[num].main(self)
            # restore our UI
            self.configure(saved_title, saved_rows, saved_cols, saved_labels, saved_commands)
            self._imports = saved_imports
            #print("finished child-module '%s' ..." % (command))
            time.sleep(0.5)
        else:
            #print("starting command '%s' ..." % (command))
            cmd_line = command
            p = subprocess.Popen(cmd_line, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
            #print("sub-process '%s' ..." % (command))

            # capture proces output and send to screen
            newlines = ['\n', '\r\n', '\r']
            out = ''
            while True:
                last = p.stdout.read(1)
                # Don't loop forever
                if last == '' and p.poll() is not None:
                    break
                while last not in newlines:
                    # Don't loop forever
                    if last == '' and p.poll() is not None:
                        break
                    out += last
                    last = p.stdout.read(1)
                self.writeline(out)
                out = ''
                time.sleep(0.1)
            if len(out):
                self.writeline(out)
            #print('finished command %s ...' % (command))
        return


def setController(existing, title, rows, cols, labels, commands):
    if existing is None:
        controller = TouchController(title, rows, cols, labels, commands)
    else:
        controller = existing
        controller.configure(title, rows, cols, labels, commands)
    return controller

'''
def runexit(command):
    print("launching command '%s' ..." % (command))
    time.sleep(0.5)
    ## run it ##
    #cmd_line = ('python %s.py' % (command))
    cmd_line = command
    p = subprocess.Popen(cmd_line, shell=True, start_new_session=True)
    print("spawned '%s' ..." % (command))
    return
'''
