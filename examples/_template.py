'''
UART communication on Raspberry Pi using Pyhton
http://www.electronicwings.com
'''

import time
import subprocess
import serial
import io
from touchcontroller import TouchController, setController


# create our UI interface
# Title Length is about 14 characters

# WARNING: labels and commands currently do not support whitespace
#time.sleep(0.5)

def main(global_controller):
    labels = ('NOOP', 'Done')
    commands = ('python -u _noop.py', 'exit')
    controller = setController(global_controller, 'template', 1, 2, labels, commands)
    controller.loop()

# call our main functions
if __name__ == '__main__':
    print ('runnign in standalone mode')
    main(None)
